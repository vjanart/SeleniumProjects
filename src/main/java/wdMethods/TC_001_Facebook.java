package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_001_Facebook extends SeMethods{
	@BeforeClass
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription ="Facebook";
		category = "Smoke";
		author= "vino";
	}
	@Test (invocationCount=2,invocationTimeOut=30000)
	public void login() throws InterruptedException {
		startApp("chrome", "https://www.facebook.com/");
		WebElement eleUserName = locateElement("id", "email");
		type(eleUserName, "vinodhini.j88@gmail.com");
		WebElement elePassword = locateElement("id","pass");
		type(elePassword, "walmart@123");
		WebElement eleLogin = locateElement("id","loginbutton");
		click(eleLogin);
		WebElement searchText = locateElement("xpath","//input[@name='q']");
		type(searchText,"TestLeaf");
		WebElement searchtext = locateElement("xpath","//ul[@id='facebar_typeahead_view_list']");
		clickWithNoSnap(searchtext);
		WebElement searchIcon = locateElement("xpath","//button[@data-testid='facebar_search_button']");
		click(searchIcon);
		WebElement Likebutton = locateElement("xpath","(//button[@type='submit'])[2]");
		
		
		String likeText=Likebutton.getText();
		if (likeText.equalsIgnoreCase("like"))
		{
			click(Likebutton);
		}
		else
			System.out.println(likeText);
		
		WebElement linkText=locateElement("xpath","//div[text()='TestLeaf']");
		click(linkText);
		Thread.sleep(7000);

		System.out.println(driver.getTitle());


		WebElement likecount = locateElement("xpath","//div[contains(text(),'people like this')]");  
		System.out.println(likecount.getText());


	}
}