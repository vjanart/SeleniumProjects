package week3.day1;




import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");	
		Select dd=new Select(src);
		dd.selectByVisibleText("Conference");

		WebElement secondSrc = driver.findElementById("createLeadForm_marketingCampaignId");
		Select secDd=new Select(secondSrc);
		secDd.selectByValue("CATRQ_CARNDRIVER");

		List<WebElement> allOptions=secDd.getOptions();
		for (WebElement eachOption:allOptions)
		{
			System.out.println(eachOption.getText());
		}








	}

}
