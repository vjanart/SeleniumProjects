package week5.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import wdMethods.*;

public class TC003_MergeLead extends ProjectMethods{
	
	@Test(enabled=false)
	public void MergeLead() throws InterruptedException {
		
		
		/*WebElement link0 = locateElement("linkText","CRM/SFA");
		click(link0);*/
		
		WebElement link1 = locateElement("linkText","Leads");
		click(link1);
		
		WebElement link2 = locateElement("linkText","Merge Leads");
		click(link2);
		
		WebElement link3 = locateElement("xpath","//input[@id='partyIdFrom']/following::img");
		click(link3);
		
		switchToWindow(1);
		WebElement eleLeadId = locateElement("xpath","//div[@class='x-form-element']/input[@name='id']");
		type(eleLeadId, "10658");
		
		WebElement link4 = locateElement("xpath","//button[text()='Find Leads']");
		click(link4);
				
		Thread.sleep(3000);
		WebElement link5 = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		//click(link5);
		clickWithNoSnap(link5);
		Thread.sleep(2000);
		
		switchToWindow(0);
		WebElement link6 = locateElement("xpath","//input[@id='partyIdTo']/following::img");
		click(link6);
		
		switchToWindow(1);
		WebElement eleLeadId1 = locateElement("xpath","//div[@class='x-form-element']/input[@name='id']");
		type(eleLeadId1, "10666");
		
		WebElement link7 = locateElement("xpath","//button[text()='Find Leads']");
		click(link7);
		
		Thread.sleep(3000);	
		WebElement link8 = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		//click(link8);
		clickWithNoSnap(link8);
		Thread.sleep(2000);
		
		switchToWindow(0);
		WebElement Merge = locateElement("linkText","Merge");
		takeSnap();
		clickWithNoSnap(Merge);
		
		//Thread.sleep(2000);
		acceptAlert();
		Thread.sleep(2000);
		
		WebElement link9 = locateElement("linkText","Find Leads");
		click(link9);
		
		WebElement eleLeadId2 = locateElement("name","id");
		type(eleLeadId2, "10658");
		
		WebElement link10 = locateElement("xpath","//button[text()='Find Leads']");
		click(link10);
		
		Thread.sleep(3000);
		WebElement ele = locateElement("xpath","//div[@class='x-paging-info']");
		getText(ele);
		
		takeSnap();
			
		
	}
	
	
	

}
