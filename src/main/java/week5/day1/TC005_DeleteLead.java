package week5.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import wdMethods.*;

public class TC005_DeleteLead extends ProjectMethods{

	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "TC005";
		testCaseDescription ="Delete a lead";
		category = "Smoke";
		author= "Vino";
		excelFileName="deletelead";
	}

	@Test(groups="regression",dataProvider="fetchData")
	public void DeleteLead(String num) throws InterruptedException
	{
		/*WebElement link1 = locateElement("linkText","CRM/SFA");
		click(link1);
		 */
		//login();
		WebElement linkone = locateElement("xpath","//div[@for='crmsfa']");
		click(linkone);
		WebElement linkTwo = locateElement("xpath","//a[text()='Leads']");
		click(linkTwo);

		WebElement linkThree = locateElement("xpath","//a[text()='Find Leads']");
		clickWithNoSnap(linkThree);

		/*WebElement link2 = locateElement("linkText","Leads");
		click(link2);

		WebElement link3 = locateElement("linkText","Find Leads");
		click(link3);*/

		WebElement link4 = locateElement("xpath","//span[@class='x-tab-strip-text '][text()='Phone']");
		click(link4);

		Thread.sleep(2000);
		/*WebElement link5 = locateElement("xpath","//input[@class=' x-form-text x-form-field '][@name='phoneCountryCode']");
		link5.clear();
		type(link5,"91");

		WebElement link6 = locateElement("xpath","//input[@class=' x-form-text x-form-field'][@name='phoneAreaCode']");
		type(link6,"001");*/

		WebElement link7 = locateElement("name","phoneNumber");
		type(link7,num);
		Thread.sleep(2000);
		WebElement link8 = locateElement("xpath","//button[@class='x-btn-text'][text()='Find Leads']");
		click(link8);

		Thread.sleep(3000);

		WebElement link9 = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		String deletetext=link9.getText();
		click(link9);


		WebElement link10 = locateElement("xpath","//a[@class='subMenuButtonDangerous'][text()='Delete']");
		click(link10);

		WebElement link11 = locateElement("xpath","//a[text()='Find Leads']");
		Thread.sleep(3000);
		click(link11);

		WebElement link12 = locateElement("xpath","//input[@class=' x-form-text x-form-field'][@name='id']");
		type(link12,deletetext);

		WebElement link13 = locateElement("xpath","//button[@class='x-btn-text'][text()='Find Leads']");
		click(link13);

		WebElement link14 = locateElement("xpath","//div[@class='x-paging-info']");
		getText(link14);

		/*takeSnap();*/

		/*closeBrowser();*/


	}




}
