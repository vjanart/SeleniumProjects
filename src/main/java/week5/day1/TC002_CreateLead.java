
package week5.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.*;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription ="Create a lead";
		category = "Smoke";
		author= "Koushik";
		excelFileName="createlead";
	}
	
	@Test(groups="smoke",dataProvider="fetchData")
	public void createLead(String cName,String fName,String lName) {
		//login();
		WebElement link1 = locateElement("xpath","//div[@for='crmsfa']");
		click(link1);
		WebElement eleCrateLead = locateElement("link","Create Lead");
		click(eleCrateLead);
		WebElement eleCN = locateElement("id","createLeadForm_companyName");
		type(eleCN,cName);
		WebElement eleFN = locateElement("id","createLeadForm_firstName");
		type(eleFN,fName);
		WebElement eleLN = locateElement("id","createLeadForm_lastName");
		type(eleLN,lName);
		WebElement eleCreateLeadButton = locateElement("class","smallSubmit");
		click(eleCreateLeadButton);

	}
	

	/*@DataProvider(name="createLead")
	public Object[][] getdata( )
	{
		Object [][] data=new Object[2][3];
		data[0][0]="CTS";
		data[0][1]="Vinodhini";
		data[0][2]="J";		
		data[1][0]="CTS";
		data[1][1]="Sangee";
		data[1][2]="suk";
		return data;
	}*/
	
}