package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {

	public static void main(String[] args) throws IOException {
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/Report.html");//It will generate the object html and report is not-editable
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);//To make the report editable
		ExtentTest test=extent.createTest("TC001_CreateLead","Create Lead");
		test.assignAuthor("Vino");
		test.assignCategory("Smoke");
		test.pass("Data DemoSalesManager is entered successfully",
				MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
		test.pass("Data crmfsa is entered successfully");
		test.fail("Login button is not clicked");
		extent.flush();
						
		
		
	}

}




