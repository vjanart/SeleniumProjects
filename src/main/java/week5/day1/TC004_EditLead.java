package week5.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import wdMethods.*;

public class TC004_EditLead extends ProjectMethods{
	
	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "TC004";
		testCaseDescription ="Edit lead";
		category = "Smoke";
		author= "Vino";
		excelFileName="editlead";
	}
	
	@Test(groups="sanity",dependsOnMethods="week5.day1.TC002_CreateLead.createLead",dataProvider="fetchData")
	public void EditLead(String cName, String fName) throws InterruptedException {
		
		/*WebElement link1 = locateElement("linkText","CRM/SFA");
		click(link1);*/
		
		//login();
		WebElement link1 = locateElement("xpath","//div[@for='crmsfa']");
		click(link1);
		WebElement link2 = locateElement("xpath","//a[text()='Leads']");
		click(link2);
		
		WebElement link3 = locateElement("xpath","//a[text()='Find Leads']");
		clickWithNoSnap(link3);
		
		WebElement link4 = locateElement("xpath","//div[@class='x-form-item x-tab-item']/div[@class='x-form-element']/input[@name='firstName']");
		type(link4,fName);
		
		WebElement link5 = locateElement("xpath","//button[@class='x-btn-text'][text()='Find Leads']");
		click(link5);
		
		Thread.sleep(3000);
		
		WebElement link6 = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		click(link6);
		
		boolean verifyTitle = verifyTitle("View Lead | opentaps CRM");
		if(verifyTitle)
		{
		System.out.println("Title is verified and correct");
		}
		else
		{
			System.out.println("Title is incorrect");
		}
					
		WebElement link7 = locateElement("id","viewLead_companyName_sp");
		getText(link7);
		/*String beforeChange = link8.getText();
		System.out.println("Company name before change: " +beforeChange);*/
		
		WebElement link8 = locateElement("xpath","//a[text()='Edit']");
		click(link8);
		
		WebElement link9 = locateElement("id","updateLeadForm_companyName");
		link9.clear();
		type(link9,cName);
		
		WebElement link10 = locateElement("xpath","//input[@class='smallSubmit'][@value='Update']");
		click(link10);
		
		WebElement link11 = locateElement("id","viewLead_companyName_sp");
		getText(link11);
	/*	String afterChange = link10.getText();
		System.out.println("Company name after change: " +afterChange);*/
		
		takeSnap();
		
			
	}
	
	/*@DataProvider(name="fetchlead")
	public Object[][] getdata()
	{
		Object [][] data=new Object[2][2];
		data[0][0]="Infy";
		data[0][1]="Vinodhini";
			
		data[1][0]="wipro";
		data[1][1]="Sangee";
		
		return data;
	}*/

}
