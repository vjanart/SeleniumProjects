package week5.day1;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import wdMethods.*;

public class ProjectMethods extends SeMethods {
	
	
	
	@DataProvider(name="fetchData")
	public Object[][] getdata() throws IOException
	{
		Object[][] excelData = ReadExcel.getExcelData(excelFileName);
		return excelData;		
	}
	
	@BeforeMethod(groups="common")
	@Parameters({"browser","appUrl","userName","password"})
	public void login(String browser, String appUrl, String userName,String password) {
		
		startApp(browser, appUrl);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, userName);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);*/
		}
	
	@AfterMethod(groups="common")
	public void close()
	{
		closeBrowser();
	}
	
	}
